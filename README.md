# Validio assignments
Hurray! If you are reading this, it means that you have gotten quite far in our recruitment process. Unless you are a web scraper or random internet nomad, in which case you may proceed exploring the big interwebz. If not, welcome! \\(^-^)/

In this repository you will find two assignments operating on the same data. Search your feelings and you know which one is for you:
- [Developer Assignment](./DEVELOP.md)
- [Data Engineer Assignment](./DATA_ENGINEER.md)
