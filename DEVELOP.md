# Validio Developer Assignment: Sportswear Insights
Welcome to Validio. We provide high quality data monitoring and validation.
As our newest rock star, you have been tasked with building a new type of data
validation system for Sportswear Insights, an important customer.

Sportswear Insights is a company that analyses clothes for various sports.
They have a state of the art machine learning system that outputs data points
of the following form:

```
{
    "id": Integer // Datapoint id
    "city": String // One of 46 european capitals
    "sport": String // One of 45 sports
    "size": String // "S", "M", "L", "XL" or "XXL"
    "feature": List[Float] // 3 dimensional vector output from ML pipeline (Computer Vision + NLP)
}
```

The data engineers at Sportswear Insights has a downstream system that consumes
and prepares this data for their data scientists. However, they are currently required
to manually clean their data before analysing it.

The ML system automatically produces raw data in a file `raw.csv`. The data scientists also
maintain a file of reference data called `reference.csv`. They regularly check the file `raw.csv`
for outliers and put the cleaned data in a file called `clean.csv`, and the outliers in a file called
`anomalies.csv`.

Your task is to automate this process.

## Specification
The system you build should:

* Be configurable with paths to a raw data file (`raw.csv`) and a reference data file (`reference.csv`).
* React to changes in both of these files.
* Performed a partitioned analysis on the data in `raw.csv` based on the reference data.
* Append data to two files `clean.csv` and `anomalies.csv`.
* Run inside a docker container for easy deployment.
* Be robust towards errors in terms of unexpected data.

### Partitioned analysis
A partition of the data is a unique combination of "sport", "size" and "city". For example ("amsterdam", "soccer", "XL") is a partition. The analysis is a three dimensional [Z-test](https://en.wikipedia.org/wiki/Z-test) for each partition, where the mean and standard deviation are computed for the reference data.

In essence the procedure is:

* For each point in the raw data, compare each dimension of the "feature" vector with the mean and standard deviation of that dimesion in the reference data for that partition.
* If the Z score exceeds 3 for any dimension, append the point to `anomalies.csv`.
* Else, append it to `clean.csv`.

### When the reference data changes
Whenever the reference data file is updated, any subsequent data analysis should be compared with the new reference data.

### When the raw data changes
The raw data file can be changed in two ways:

* New values can be appended to the current file.
* The whole file can change.

If new values are appended, only the new values should be processed. If the whole file is changed, the whole file is processed.

### Performance requirements
On any normal laptop the system should:

* Be able to process reference datasets up to 1M datapoints.
* Have low latency for small files: raw datasets under 10K datapoints should be processed under 500ms.
* Have high throughput: raw datasets up to 1M datapoints should be processed in under 60s.

## Example data
Example data are provided in the `example_data` directory.

## Third party libraries
Third party libraries are okay to include in your solution, as long as they have appropriate licenses for commercial use (Apache 2, MIT, etc.).

# Evaluation
Please submit your code to `marten@validio.io` upon completion. We will schedule a follow-up session where you get to present your solution.

Your solution should be able to run with a docker invocation like:
```
docker run -v /test/data:/mnt/testdata https://www.some.registry/your/docker/container --reference /mnt/testdata/test_ref.csv --raw /mnt/testdata/test_raw.csv --output_dir /mnt/testdata/output/
```
The configuration does not have to be CLI args. You are free to choose any convenient way to configure the application, as long as it will be easy to use.

## How does a good solution look like?
If the answer is yes to the following questions, we will most likely be happy with your solution:

* Does the solution comply with the specifications?
* Is the solution easy to run and configure?
* Does the solution perform well on our test cases?
* Is the code readable and easy to grasp?
* Can you deliver a good presentation of your solution?

# Accepted programming languages
We recommend building this solution in Rust or Go, but any language is accepted.

# Questions?
Feel free to reach out to `marten@validio.io` with any questions about the task and requirements.
