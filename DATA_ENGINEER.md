# Validio Data Engineering Assignment: Sportswear Insights
Welcome to Validio. We provide high quality data monitoring and validation.

We have recently built and shipped a data validation system for Sportswear Insights (an important customer). The specifications of the system can be found [here](./DEVELOP.md).

The data validation works great, but unfortunately it turns out that the internal system that prepares the data at Sportswear Insights is unreliable. Therefore, they have asked us to deploy our solution directly against their own data sources.

Your mission, should you choose to accept it, is to build a data preparation system that produces these csv files.

## Task
Build a data preparation system that consumes data from multiple sources and appends data points to a single csv file.

## The data sources
Sportswear insights collects data through multiple scrapers that dump their data to a collection in a mongodb database. The format of the data can vary depending on which scraper that found the item, but most of the datapoints should contain enough information for you to extract a city, sport and a size. Each datapoint has a unique ID.

The machine learning team consumes this data and produces features which they output to a Postgresql table together with the ID of the associated datapoint.

You need to create a process to find all datapoints which can be parsed from the database that has a ML feature, and append these to a file called `raw.csv`. The process should continuously append new incoming data to the file.

## Example data
Example target data are provided in the `example_data` directory.

Two docker images are provided that are pre-populated with data:

- Pre-populated mongodb image: `registry.gitlab.com/marten6/sportswear-insights/mongodb:latest`. Based on version 4.2 of [mongodb](https://hub.docker.com/_/mongo)
- Pre-populated postgres image: `registry.gitlab.com/marten6/sportswear-insights/postgres:latest`. Based on version 9.6 of [postgresql](https://hub.docker.com/_/postgres)

# Evaluation
Please submit your solution to `marten@validio.io`. Your solution may be composed of multiple services or a single script. You may use any container orchestration tool to set up your system, as long as it is relatively simple to replicate on a new machine.

## What is a good solution
The following properties are marks of a good system

* It is easy to set up the system
* The dependent database docker images can easily be replaced
* The system reacts in a timely manner to new data
* The system is robust towards unexpected data

# Questions?
Feel free to reach out to `marten@validio.io` with any questions about the task and requirements.
